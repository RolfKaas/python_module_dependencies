#! /tools/bin/python3

import subprocess
import re
import os.path
import sys
import os

# Usage:


class Dependencies(dict):
    ''' The class extends dict. It is in essence a dict with prg names as keys
        and prg paths as values.
        The constructor reads a config file to load the dict and tests all
        paths for existence and makes sure they are executable. It is possible
        to set the prgs that will raise an Execution error, on default all prgs
        will raise an exception if not found.
        In addition to the key value pair: [prg, path] an additional pair will
        also be created containing a path to the directory in which the prg is
        found: [prg_dir, path].
        Example:
            Config file entry:
                blastn  /usr/bin/local/blastn

            Key value pairs created:
                key: blastn     val: /usr/bin/local/blastn
                key: blastn_dir val: /usr/bin/local
    '''

    def __init__(self, config, *essentials, executables=True):
        ''' Constructor - reads config file. Expected format:
                prg<tab>prg_path
                prg<tab>prg_path
                # comment
            Loads prg and prg_paths into dict and makes sure the prg_paths
            exist and are executable.
            Key: prg
            Val: prg_path (False if not found and not essential)
            self.host - Contains host name or 'unknown'.
            self.config - Contains path to config file.

            NOTE: The dict function __setitem__ has been overwritten in this
            class.
        '''
        try:
            self.host = subprocess.check_output(["hostname"], shell=True)
        except:
            self.host = "unknown"
        self.config = config
        with open(config, "r", encoding="utf-8") as config_fh:
            for line in config_fh:
                if(line[:1] == "#"):
                    pass
                else:
                    line = line.rstrip()

                    # Ignore empty lines
                    if(not line):
                        continue

                    # Split expects prg_name<tab>prg_path
                    prg_entry = line.split()
                    print("ENTRY: " + str(prg_entry))

                    # All entries are essential per default
                    essential_prg = True
                    # Unless an essentials list is specified, then only entries
                    # found in the list are deemed essential.
                    if(essentials):
                        if(prg_entry[0] not in essentials):
                            essential_prg = False

                    if(executables):
                        prg = self.is_executable(prg_entry[1], essential_prg)
                        if(prg):
                            self[prg_entry[0]] = prg
                        elif(not essential_prg):
                            self[prg_entry[0]] = False
                        else:
                            print("Path is not executable: ", prg_entry[1])
                            quit(1)
                    else:
                        if(os.path.islink(prg_entry[1])):
                            path = os.path.realpath(prg_entry[1])

                        if(os.path.isfile(prg_entry[1])):
                            self[prg_entry[0]] = prg_entry[1]
                        elif(not executables and os.path.isdir(prg_entry[1])):
                            self[prg_entry[0]] = prg_entry[1]
                        else:
                            print("Path does not seem to exist: "
                                  + prg_entry[1])
                            quit(1)

# created: python3  with: /home/data1/tools/bin/Anaconda3-2.5.0/bin/python3.5
# In IF:
# created: python3  with: /home/data1/tools/bin/Anaconda3-2.5.0/bin/python3.5
# created: python3_dir  with:

    def __setitem__(self, key, value, dont_create_dir=False):
        super().__setitem__(key, value)
        if(dont_create_dir is False and value):
            key_dir = os.path.dirname(value)
            self.__setitem__(key+"_dir", key_dir, dont_create_dir=True)

    @staticmethod
    def get_actual_path(path):
        ''' Returns output of which command if succeful or False.
        '''
        # If the path is given as an env variable e.g. "bwa" instead of
        # "/usr/bin/bwa". "Which" is used to get the actual path.
        try:
            which_txt = subprocess.check_output(["which " + path], shell=True)
            which_txt = which_txt.decode('utf-8')
            which_txt = which_txt.rstrip()
            return which_txt
        except subprocess.CalledProcessError:
            return False

    @classmethod
    def is_executable(cls, path, essential=True):
        ''' Class method. Uses the "file" cmd to check if a path is
            executable. If path is not found the method uses "which path" to
            see if a path can be found. If so the new path is tested with this
            method.
            Return path if found and executable, else returns False.
        '''
        # Handles symbolic links
        if(os.path.islink(path)):
            path = os.path.realpath(path)

        try:
            exe_txt = subprocess.check_output("file " + path, shell=True)
            exe_txt = exe_txt.decode('utf-8')
            if(re.search(r"executable", exe_txt)):
                return path
            elif(re.search(r"No such file or directory", exe_txt)):
                which_txt = Dependencies.get_actual_path(path)
            else:
                return False
        except subprocess.CalledProcessError:
            print("Dependencies file cmd now give an error?!", file=sys.stderr)
            which_txt = Dependencies.get_actual_path(path)
            quit(1)

        if(not which_txt and not essential):
            return False
        elif(not which_txt):
            print("The path", path, "does not seem to exist.")
            quit(1)

        # Check if the new found path is executable.
        if(cls.is_executable(which_txt, essential)):
            return which_txt
        else:
            return False


if __name__ == '__main__':
    # The module can be run by itself for debugging.

    # Create the configuration file used for testing.
    with open("temp_conf_file", "w", encoding="utf-8") as temp_conf_fh:
        temp_conf_fh.write("less_path\t/usr/bin/less\n")
        temp_conf_fh.write("less_env\tless\n")
        temp_conf_fh.write("fantom\t/the/path/that/leads/to/nowhere\n")

    prgs = Dependencies("temp_conf_file", ("less_path", "less_env"))
    subprocess.call(["rm  temp_conf_file"], shell=True)
    print("TEST:" + os.path.basename(__file__) + ":Done", file=sys.stderr)
    quit(0)
